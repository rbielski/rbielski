<img src="https://gitlab.cern.ch/rbielski/rbielski/-/raw/master/authors2020-labelled.png" width="32%" />
<img src="https://gitlab.cern.ch/rbielski/rbielski/-/raw/master/authors2021-labelled.png" width="32%" />
<img src="https://gitlab.cern.ch/rbielski/rbielski/-/raw/master/authors2022-labelled.png" width="32%" />

*Code to make these plots: [gitlab.cern.ch/snippets/1976](https://gitlab.cern.ch/-/snippets/1976)*
